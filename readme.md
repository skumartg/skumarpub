### Template description and usage
**The project is intended to run in a managed docker containers with following three services:**

1. POSTGRESQL
2. REDIS
3. NODEJS

**Instructions to use this template service**

1. To run docker image fo this project run the following command in terminal:
    sudo docker-compose -f docker-compose.dev.yml up --build             For development
    sudo docker-compose -f docker-compose.yml up --build                 For production
   With this command the project will run cache-server,postgresql,nodejs server. Nodejs server will listen to localhost:80 or just localhost.
2. Working directory will be /service folder
3. Project config can be stored in /service/config directory. We are using config npm package to manage configs. It is mandatory to add project level
   variables inside /config  directory
4. Database connections and models can be configured inside /service/database directory
5. For the synchronous APIs, that can be used for inter process communication must be written inside /service/ipc/{version} folder
6. Inside controllers create file for each category and write function to whom router will communicate with.
7. Inside services create file to have backend logic
8. Inside drivers write drivers for each functionalties that are to be exposed to asynchronous IPC. Driver files should be written in this format:
    driver_some_functionality.js
   index.js file will dynamically import the drivers file and then can be used in /service/index.js by importing.
9. The template consists of generic modules that can be imported anywhere inside project with a require statement. The list of modules are:
    activityLogger   require('@activityLogger')
    awsLib   require('@awsLib')
    logger   require('@logger')
    api-schema-validator   require('@api-schema-validator')
    error-messages   require('@error-messages')
    cache   require('@cache')
    ipc   require('@ipc')
    dynamodb   require('@dynamodb') 
10. Public APIs can be written inside /service/public/{version} directory
11. Swagger documentation metadata should be maintained with every release inside the /service/swagger directory. Model definitions should be 
    written in definitions.js file
12. Some automation tests can be written in /service/test directory. The automation.test.js will run type checking tests for synchronous functions.
    To test functions parameters type checking, every function must call /service/utilities/validation-util.typeChecker([],[]) function. This function
    accepts two arguments, parameters recieved in the function and array of expected data types of the function parameters.
13. Utility code, constants, error tracing is written in utilities directory

**Instructions to write swagger api for api routes**

1. Developer must write swagger api doc for each api, it helps in testing.
2. To write swagger api, check /service/public/v1/index.js 
3. Swagger documentation must be written in YAML format.
4. Developer can easily generate error-free YAML by simulationg and rendering the YAML content by visiting [Swagger Editor](https://editor.swagger.io/)

