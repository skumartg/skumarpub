/** 
 * @info : Below module lets you alias your personal build libraries
*/
require('module-alias/register')

const app            = require("express")();
const bodyParser     = require("body-parser");
const cookieParser   = require("cookie-parser");
const config         = require("config");
const bearer         = require("bearer");
const multer         = require("multer");
const helmet         = require('helmet')
const publicAPI      = require("./public/index.js");
const ipcAPI         = require("./ipc/index.js");
const http           = require('http').Server(app);
let APP_SECRET       = config.get("apiKey");
const crypto         = require('crypto');
const swaggerUi      = require('swagger-ui-express');
const swaggerJSDoc   = require('swagger-jsdoc');    // initialize swagger-jsdoc
const swaggerOptions = require(`./swagger/${config.get("environment")}`);
var swaggerSpec      = swaggerJSDoc(swaggerOptions);
const trace          = require('@trace-lib');

let {logger, ExtendedLogger} = require("@logger")
// const client        = require('@cache');
// const cache= new client("localhost");

let expressLogger = new ExtendedLogger("express.server");
expressLogger.log(`Initializing in ${config.get("environment")} environment..........`)
// expressLogger.log("ABC");     jjhfjkdhf
/*
  apply all the middlewares
*/
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))  
app.use(helmet());
process.on("unhandledRejection", function(reason, p) {
  trace();
  new ExtendedLogger("unhandlePromiseRejection").log("Promise unhandled and rejecting due to reason '" + reason +"'", 'warn');
});

function verifyRequestSignature(req, res, buf) {
  let extendedLogger = new ExtendedLogger(null, req);
  if(config.get("environment") != "production"){
    req.noValidSignature = false;
    extendedLogger.warn(`IPC verified signature not called because environment is ${config.get('environment')}`);
    return;
  }

  
  var signature = req.headers["x-hub-signature"];
  if (!signature) {
      extendedLogger.log("Couldn't validate the signature.", "error");
      req.noValidSignature = true;
  } else {
    var elements = signature.split('=');
    var method = elements[0];
    var signatureHash = elements[1];

    var expectedHash = crypto.createHmac('sha1', APP_SECRET)
                        .update(buf)
                        .digest('hex');

    if (signatureHash != expectedHash) {
      extendedLogger.log("Couldn't validate the request signature.", "error");
      req.noValidSignature = true;
    }
  }
}

app.use("/ipc/", bodyParser.json({verify : verifyRequestSignature})); //Need to validate signature from trilyo server

// parse application/json
app.use(bodyParser.json())

// parse cookies from the request and get req.cookies
app.use(cookieParser());

const logInserter = function(req, res, next) {
  if (!req.noValidSignature) {
    let startTime = new Date().getTime();
    let url = req.originalUrl.split("?")[0];
    req.log = function(message, level = "info") {
      let messageLocal = message;
      if (messageLocal instanceof Object) {
        messageLocal = JSON.stringify(messageLocal, null, 4)
      }

      logger.log({
        "level"   : level,
        "message" : messageLocal,
        "label"   : req.method.toLowerCase() + "." + (url == "" || url == "/") ? "home" :  url.replace(/\//gi, ".").substring(1)
      })
    }
    req.log(`req ${url} body: ${JSON.stringify(req.body)}, query: ${JSON.stringify(req.query)}`)
    res.on("finish", function() {
        let logLevel = "info";
        if (this.statusCode != 200 && this.statusCode != 304) {
          logLevel = "warn";
        }
        req.log(`Status:${res.statusCode}, ${req.method.toUpperCase()} ${url} finished processing in ${(new Date().getTime() - startTime) / 1000}s`, logLevel);
    })
    next();
  } else {
    res.status(400).send({"status" : "failed", "message" : "Invalid Signature"});
  }
}

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));

app.use("/api/", logInserter, publicAPI);
app.use("/ipc/", logInserter, ipcAPI);

// console.log(process.env);
app.set('port', (process.env.PORT || 3000));

 
/*
  last route
*/
app.get('*', logInserter, function(req, res){
  res.status(404).send('what???');
});
http.listen(app.get('port'), function() {
  expressLogger.log(`Server running on ${app.get('port')}`);
});
/*
starting event loop driver for listening to sqs  
*/
const drivers = require(`./ipc/${config.driverVersion}/drivers`);
const activityLogger = require('@activityLogger');
// console.log(cache.setVal);
// cache.setVal("foo", "bar").then(function() {
//   cache.getVal("foo");

//   //Uncomment this to start listening to queue
//   // drivers.start()

//   activityLogger({
//     "version": "1",
//     "service": "officeeditor",
//     "category": "user",
//     "actor": "123",
//     "tag": "['test','abc']",
//     "datetimestamp": "201909121625",
//     "action": "action",
//     "usertype": "use",
//     "target": "example target"
//    });
// });

const models       = require("./database/index.js");
module.exports = app;
