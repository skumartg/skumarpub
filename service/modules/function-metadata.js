/**
 * This method contains synchronous methods that are required to be tested for parameter type checking
 */
module.exports = {
    "modules":{
        "ipc_v1_services_cacheHandler.js":{
            "path":"ipc/v1/services/cacheHandler",
            "fn_createRedisKey":["object"]
        }
    }
}