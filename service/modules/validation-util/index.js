const trace = require('@trace-lib');
const logger = require('@logger');
const typeMismatchError = require('@custom-errors').FunctionTypeMismatchException;
const dataTypes = ["undefined","symbol","string","object","number","function","boolean","?string","?number","?boolean","?function","?object","?symbol"];

/**
 * This function is used to type check the arguments of a function. It supports nullable arguments prefixed with '?'. 
 * @param {Array} args Original arguments received by the function
 * @param {Array} types Can be any js types among undefined , symbol , string , object , number , function , boolean , bigint , ?string , ?number , ?boolean , ?bigint , ?function , ?object , ?symbol 
 */
var typeChecker = (args,types)=> {
    let errors = [], argumentCount = args.length;

    if(!args || !types){
        errors.push(`typeChecker invoked improperly. args or types is missing`);
    } else if(!Array.isArray(args) || !Array.isArray(types)){
        errors.push(`typeChecker invoked improperly. args and types should be an array`);
    } else if(args.length != types.length){
        errors.push(`typeChecker invoked improperly. Expecting same array length in both parameters`);
    } else {
        types.map((type) => {
            if(!dataTypes.includes(type)){
                // console.log("Found mismatch");
                errors.push(`${type} is not a defined type in application`);
            }
        });

        // If there are no errors so far then only further type checking is performed
        if(errors.length == 0){
            for(let index=0; index < argumentCount ; index++){
                let isNullableParam = types[index].startsWith("?");
                let nullCheckCondition = isNullableParam && args[index] != null && typeof(args[index]) != types[index].substr(1);
                let notNullCheckCondition = !isNullableParam && typeof(args[index]) != types[index];

                if(nullCheckCondition || notNullCheckCondition){
                    errors.push(`got ${typeof(args[index])} instead of ${types[index]} at index ${index}`);
                }
            }
        }
    }

    if(errors.length > 0){
        new logger.ExtendedLogger("validation-util.js typeChecker()",null).log(`Type mismatch error, stack trace:`,trace());
        new logger.ExtendedLogger("validation-util.js typeChecker()",null).debug(`Type mismatch error, stack trace:`,trace());
        throw new typeMismatchError(errors);
    }
}

module.exports = {
    typeChecker
}