module.exports = (QueueUrl, MaxNumberOfMessages, WaitTimeSeconds) => {
    return {
        QueueUrl,
        MaxNumberOfMessages,
        WaitTimeSeconds
    }
}