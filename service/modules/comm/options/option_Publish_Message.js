module.exports = (MessageBody, QueueUrl, MessageGroupId) => {
    return {
        MessageBody,
        QueueUrl,
        MessageGroupId
    }
}