module.exports = (region, accessKeyId, secretAccessKey, apiVersion) => {
    return {
        region,
        accessKeyId,
        secretAccessKey,
        apiVersion
    }
}