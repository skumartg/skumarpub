const config              = require("config");
const request             = require("request");
const crypto              = require("crypto");
const aws                 = require("aws-sdk");
const ipcSettings         = config.get("services");
const sqsCacheUrl         = config.get("sqsRedisLogUrl");
const {ExtendedLogger}    = require('@logger');
const options             = require('./options');
const { IpcHelper }       = require('./helper');
const Client              = require('@cache');
const uuidv4              = require('uuid/v4');
const uuidv1              = require('uuid/v1');
const DynamoDb            = require('@dynamodb');

/**
 * Class to handle inter process communication
 * @author Swapnil Kumar(techgenies.com)
 */
class Ipc {

  constructor(service){
    this._service      = service;
    this._ipcConstants = ipcSettings[service].queue;
    this._ipcHelper    = new IpcHelper();
    this.cacheClient   = new Client(sqsCacheUrl);
    this._persistentStorage = new DynamoDb("sqsLog");
    let logger = new ExtendedLogger("IPC_constructor()", null);
    logger.log("created redis connection for logging IPC logs");
    logger.debug("created redis connection for logging IPC logs");
    logger.debug("queue url",this._ipcConstants.sqsQueueUrl);
  }

  /**
   * The function to communicate in synchronous manner with the api
   * @param {String} serviceName Name of service to call
   * @param {String} version Version of the service to call
   * @param {String} endPoint End point path to hit
   * @param {*} data Data to be sent in the request
   * @author Swapnil Kumar(techgenies.com)
   */
  send(serviceName, version, endPoint, data){
    let logger = new ExtendedLogger("comm_index.js_send()", null);
    var Hash       = crypto.createHmac('sha1', config.get("apiKey"))
                        .update(JSON.stringify(data))
                        .digest('hex');
                
    var reqOptions = {
      uri: `${config.get("services." + serviceName + ".url")}/ipc/${version}/${endPoint}`,
      json: data,
      method: 'POST', 
      headers : {
          'Accept': 'application/json',
          'x-hub-signature' : "sha1=" + Hash
      }
    };
    return new Promise(function(resolve, reject) {
      request(reqOptions, function(error, response, body) {
          if (!error && response.statusCode == 200) {
            logger.log(`Request to service '${serviceName}', version ${version} on endPoint '${endPoint}' was successful`);
            resolve(body);
          } else {
            logger.log(error || `Request to service '${serviceName}', version ${version} on endPoint '${endPoint}' failed with message '${body.message}'`, "error");
            reject(body.message);
          }
        });
      });
  }

  /**
 * This function publishes/sends a message on the SQS queue
 * @param {JSON} messageBody This is a JSON object containing the message body
 * @author Swapnil Kumar(techgenies.com)
 */
  async publish(messageBody){
    let logger = new ExtendedLogger("comm_index.js_publish()", null);
    logger.debug("messageBody",messageBody);
    let sqsOptions = options.optSqsConnection(this._ipcConstants.sqsRegion, this._ipcConstants.sqsAccessKeyId, this._ipcConstants.sqsSecretAccessKey, 
      this._ipcConstants.sqsApiVersion);
    logger.debug(sqsOptions);
    logger.debug("sqsOptions",sqsOptions)
    const sqs = new aws.SQS(sqsOptions);
    let sendMessageParam = options.optPublishMessage(JSON.stringify(messageBody), this._ipcConstants.sqsQueueUrl, this._ipcConstants.sqsMessageGroupId);
    let status = await this._ipcHelper.sendMessage(sqs, sendMessageParam);
    //Running redis hmset asynchronously to save info in redis
    let dateTime = new Date();
    let information = {SequenceNumber:status.SequenceNumber,DateTime: dateTime.toString()}
    this.cacheClient.hmSetVal(status.MessageId,"Publish", JSON.stringify(information));
    //Dynamo db item to be added to sqs log for persistent storage
    let dynamoDbItem = this._ipcHelper.createDynamoDbLogItem(status.MessageId, "timeStamp", dateTime.getTime().toString(), "Publish", information);
    this._persistentStorage.create(dynamoDbItem);
    logger.debug("status",status);
    return status;
  }

  /**
   * This function is used to recieve messages from SQS queue
   * @returns {AWS.SQS.ReceiveMessageResult} An object of type ReceiveMessageResult containing metadata information as well as an array of messages
   * @author Swapnil Kumar(techgenies.com)
   */
  async subscribe(){
    let logger = new ExtendedLogger("comm_index.js_subscribe()", null);
    logger.debug("Entered function");
    let sqsOptions = options.optSqsConnection(this._ipcConstants.sqsRegion, this._ipcConstants.sqsAccessKeyId, this._ipcConstants.sqsSecretAccessKey, 
      this._ipcConstants.sqsApiVersion);
    logger.debug("sqsOptions",sqsOptions)
    const sqs = new aws.SQS(sqsOptions);
    let receiveMessageParam = options.optReceiveMessage(this._ipcConstants.sqsQueueUrl, this._ipcConstants.sqsMaxNumberOfMessages, this._ipcConstants.sqsWaitTimeSeconds);
    let status = await this._ipcHelper.receiveMessage(sqs, receiveMessageParam);
    logger.debug("status",status);

    if(typeof(status.Messages) != "undefined"){
      status.Messages.map((message) => {
        //Running redis hmset asynchronously
        message.Id = uuidv4();
        let dateTime = new Date();
        let information = {ReceiptHandle:message.ReceiptHandle,TimeStamp: dateTime.toString()};
        let receiveKey = uuidv1();
        this.cacheClient.hmSetVal(message.MessageId, `Receive: ${receiveKey}`, JSON.stringify(information));
        let dynamoDbItem = this._ipcHelper.createDynamoDbLogItem(message.MessageId, "timeStamp", dateTime.getTime().toString(), "Receive", {receiveKey, information});
        this._persistentStorage.create(dynamoDbItem);
      });
    } else {
      status.Messages = [];
    }
  
    logger.debug("status",status);
    return status;
  } 

  /**
   * This function will delete the message from SQS queue based on the received message object
   * @param {AWS.SQS.ReceiveMessageResult} messageResponse  The message response object received from the subscribe request
   * @returns {AWS.SQS.DeleteMessageBatchResult}  This function returns a delete message batch result object containing Success, Failed, ResponseMetadata information
   * @author Swapnil Kumar(techgenies.com)
   */
  async finish (messageResponse){
    let logger = new ExtendedLogger("comm_index.js_finish()", null);
    logger.debug("messageHandle",messageResponse);
    if(typeof(messageResponse.Messages) == "undefined" || messageResponse.Messages.length == 0){
      return null;
    }

    let entries = this._ipcHelper.getMessageHandles(messageResponse.Messages);
    logger.debug("entries",entries);
    let sqsOptions = options.optSqsConnection(this._ipcConstants.sqsRegion, this._ipcConstants.sqsAccessKeyId, this._ipcConstants.sqsSecretAccessKey, 
      this._ipcConstants.sqsApiVersion);
    logger.debug("sqsOptions",sqsOptions);
    const sqs = new aws.SQS(sqsOptions);
    let deleteMessageParam = options.optFinishMessage(this._ipcConstants.sqsQueueUrl, entries);
    logger.debug("deleteMessageParam",deleteMessageParam)
    let status = await this._ipcHelper.deleteMessage(sqs, deleteMessageParam);
    logger.debug("status",status);

    //Saving results to redis cache for logging message queues
    messageResponse.Messages.map((message) => {
      //Possible optimization to use map here, but since messages count cannot be more than 10, so moving on with .filter()
      let isSuccess = status.Successful.filter( item => {return item.Id == message.Id }).length > 0;
      message.SuccessDeletion = isSuccess;
      let dateTime = new Date();
      message.TimeStamp = dateTime.toString();
      let information = this._ipcHelper.pickMessagePropertyForSavingInRedis(message);
      this.cacheClient.hmSetVal(message.MessageId,"Delete", JSON.stringify(information));
      let isError = typeof(message["HasErrors"]) == "undefined" ? "false" : "true";
      let dynamoDbItem = this._ipcHelper.createDynamoDbLogItem(message.MessageId, "timeStamp", dateTime.getTime().toString(), "Delete", information, "isError", isError);
      this._persistentStorage.create(dynamoDbItem);
    });

    return status;
  }

}

module.exports = Ipc;