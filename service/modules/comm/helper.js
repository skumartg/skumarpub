/**
 * Helper class for Ipc
 * @author Swapnil Kumar(techgenies.com)
 */
class IpcHelper {
  constructor(){
  }

  /**
   * Creates an array by picking the ReceiptHandle from the array of json
   * @param {Object} messages An object containing Messages array of JSON
   * @returns {Array} An array of JSON containing Id made from uuidv4 and ReceiptHandle from message
   * @author Swapnil Kumar(techgenies.com)
   */
  getMessageHandles(messages){
    let entries = []
    messages.map((message) => {
      entries.push({Id:message.Id, ReceiptHandle: message.ReceiptHandle});
    });
    return entries;
  }

  /**
   * The function will send a message to the aws sqs queue on queue url
   * @param {AWS.SQS} sqs The sqs connection object filled with access key and secret key
   * @param {AWS.SQS.SendMessageRequest} params The send message parameters containing MessageBody, QueueUrl, MessageGroupId
   * @author Swapnil Kumar(techgenies.com)
   */
  sendMessage(sqs, params){
    return new Promise((resolve,reject) => {
      sqs.sendMessage(params, (err, data) => {
        if(err){
          reject(err);
          return;
        }
        resolve(data);
      })
    });
  }

  /**
   * The function will recieve messages from queue url
   * @param {AWS.SQS} sqs The sqs connection object, filled with access key and secret key
   * @param {AWS.SQS.ReceiveMessageRequest} params The recieve message parameter containing QueueUrl
   * @author Swapnil Kumar(techgenies.com)
   */
  receiveMessage(sqs, params){
    return new Promise((resolve, reject) => {
      sqs.receiveMessage(params, (err, data) => {
        if(err){
          reject(err);
          return;
        }
        resolve(data);
      })
    });
  }

  /**
  * 
  * @param {AWS.SQS} sqs The sqs connection object, filled with access key and secret key
  * @param {AWS.SQS.DeleteMessageRequest} params The delete message parameter containing QueueUrl, MessageHandle
  * @author Swapnil Kumar(techgenies.com)
  */
  deleteMessage(sqs, params){
      return new Promise((resolve, reject) => {
        sqs.deleteMessageBatch(params, (err, data) => {
          if(err){
            reject(err);
            return;
          }
          resolve(data);
        })
      })
    }

  /**
   * Function will remove Body and MessageId properties to hide from logs
   * @param {AWS.SQS.ReceiveMessageResult} obj Message object received in the subscribe request with added properties
   * @returns {Object} Returns an object by setting Body and MessageId to null
   * @author Swapnil Kumar(techgenies.com)
   */
  pickMessagePropertyForSavingInRedis(obj){
    obj.Body = "null";
    return obj;
  }

  /**
   * Function to create a sqs log table to be inserted
   * @param {String} message_id MessageID received from SQS
   * @param {String} hasErrors A string having "true" or "false"
   * @param {Number} timeStamp Epoch timestamp
   * @param  {...any} args Field-Value pairs
   */
  createDynamoDbLogItem(message_id, ...args){
    let retObj = {
      id: message_id
    };
    let len = args.length;
    for(let index = 0; index < len; index+=2){
      retObj[args[index]] = args[index + 1];
    }
    return retObj;
  }

}

module.exports = {
  IpcHelper
}