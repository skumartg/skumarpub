const Ipc = require('@ipc');
const config = require('config');
const constants = config.get("services");
/**
  * Function that calls activity logger queue and publishes message to it
  * @param {*} data Data to log in activity logger
  * @author Swapnil Kumar(techgenies.com)
  */
const log = async (data) => {
    let ipc = new Ipc("activityLogger");
    let body = {method: constants.activityLogger.queue.methods.save, body: data};
    await ipc.publish(body);
}

module.exports = log