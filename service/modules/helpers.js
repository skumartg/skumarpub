/**
 * Function checks whether the passed string is a valid json or not
 * @param {String} stringBody String to be checked whether it is JSON or not
 * @returns {*} Returns json if it is a valid JSON, otherwise null
 * @author Swapnil Kumar(techgenies.com)
 */
const tryParseJson = (stringBody) => {
    try
    {
        return JSON.parse(stringBody);
    }
    catch(err)
    {
        return null;
    }
}

/**
 * Function will assign val to the key if the key is undefined or does not exist in the obj
 * @param {Object} obj Object to check whether the key exists or not
 * @param {String} key Key to find in object
 * @param {*} val Value to put in the object for key
 * @author Swapnil Kumar(techgenies.com)
 */
const assignValueIfUndefined = (obj, key, val) => {
    if(typeof(obj[key]) == "undefined"){
        obj[key] = val;
    }
}

/**
 * Function to simulate validation function for body of ipc request. Useful for inter microservices communication validation
 * @param {Function} validator Function that is to be executed inside of this handler
 * @param {Object} validationCriteria A JSON object containing field properties and validation schema
 * @author Swapnil Kumar(techgenies.com)
 */
const validatorHandler = (validator, req, validationCriteria) => {
    
    let {isValid, errorMessage} = validator(req, validationCriteria);
    
    if(isValid){
        return null;
    }
    return errorMessage;
}

/**
 * Function to add error tags to incoming IPC message
 * @param {Object} obj Object on which to attach the error variables
 * @author Swapnil Kumar(techgenies.com)
 */
const addErrorToIpcRequest = (obj, message) => {
    //Adding a check to message to flag this message as not storable
    obj.HasErrors = true;
    assignValueIfUndefined(obj,"Errors",[]);
    obj.Errors.push(message);
}

/**
 * Funciton to convert snakcase string to  camelcase string
 * @param {String} str String in snakeCase
 * @author Swapnil Kumar(techgenies.com)
 */
const snakeToCamel = (str) => str.replace(
    /([-_][a-z])/g,
    (group) => group.toUpperCase()
                    .replace('-', '')
                    .replace('_', '')
);

module.exports = {
    tryParseJson,
    assignValueIfUndefined,
    validatorHandler,
    addErrorToIpcRequest,
    snakeToCamel
}