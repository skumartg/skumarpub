/* jshint strict: false */
var stackTrace = require('./stack-trace');

/**
 * This function returns caller for the func sent in the parameter
 * @param {function} func The function for which caller needs to be returned
 * @returns {String} name of the caller of the function
 */
function getCaller(func) {
    return func.caller;
}

/**
 * This function returns the complete stack trace of the called function
 * @param {Function} func The function for which to get the stacktrace from
 * @returns {Array} An array containing metadata of every function call in the stack trace
 */
function getData(func) {
    let trace = stackTrace.get(func || getCaller(getData));
    let calls = [];

    trace.map(function(caller){
        calls.push({
            typeName: caller.getTypeName(),
            functionName: caller.getFunctionName(),
            methodName: caller.getMethodName(),
            filePath: caller.getFileName(),
            lineNumber: caller.getLineNumber(),
            topLevelFlag: caller.isToplevel(),
            nativeFlag: caller.isNative(),
            evalFlag: caller.isEval(),
            evalOrigin: caller.getEvalOrigin()
        });
    });

    return calls;
}

module.exports = {
    getData
};