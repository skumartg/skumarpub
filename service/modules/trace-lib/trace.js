const callerid = require('./util');
const crypto = require('crypto');
const logger = require('@logger')
const blockedStackTraceNames = ['node_modules','events.js','task_queues.js','stream_readable.js'];
const config = require('config');
const validationUtil = require('@validation-util');
const functionMetadata = require('@function-metadata')["modules"];

const randomNumberMultiplier = 10000;
const offsetForUniqueSubstring = 10;
const maxTraceCount = 10;

const moduleName = "utilities_trace-lib_trace.js";

/**
 * @param {String} message string required to send for creating a unique id for stack trace logs
 * @returns sha256 of message parameter
 * @example if message is 'abcdef', then it will return 'bef57ec7f53a6d40beb640a780a639c83bc29ac8a9816f1fc6c5c6dcd93c4721'
 */
var createUniqueId = function(message){
    return crypto.createHmac('sha256', config.get("apiKey"))
    .update(message)
    .digest('hex');
}

/**
 * This function will print stack trace of the function call stack till the point this function is invoked in the code.
 */
var globalTrace = function (){
    let timestamp = new Date().getTime().toString() + createUniqueId(Number(Math.random() * randomNumberMultiplier).toString()).substr(offsetForUniqueSubstring);
    let calls = callerid.getData(), trace = "", traceCount = 0, fullTrace = "";

    for(let index = 0 ; index < calls.length; index++){
        
        if(traceCount < maxTraceCount){
            let isblocked = false;
            blockedStackTraceNames.map((blockedName) => {
    
                if(isblocked) { return; }
    
                if(calls[index].filePath != null && calls[index].filePath.includes(blockedName)){
                    isblocked = true;
                }
    
            });
    
            if(!isblocked){
                traceCount++;
                trace = `${calls[index].functionName}->${trace}`;
            }
        }
        fullTrace = `at ${calls[index].filePath}, ${calls[index].functionName}(), line ${calls[index].lineNumber}\n${fullTrace}`;
    }

    new logger.ExtendedLogger("trace.js globalTrace()",null).log(`${timestamp}\n${fullTrace}`);
    new logger.ExtendedLogger("trace.js globalTrace()",null).debug(`${timestamp}\n${fullTrace}`);

    return `${timestamp}\n${trace.substr(0,trace.length - 2)}`
}

module.exports = {
    globalTrace,
    createUniqueId
}