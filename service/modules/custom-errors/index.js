/**
 * A custom exception class for type checking exceptions in function calls
 */
class FunctionTypeMismatchException extends Error {  
    constructor (message) {
      super(message);
  
      this.name = this.constructor.name;
      this.status = 500;
    }
  
    statusCode() {
      return this.status;
    }
  }
  
module.exports = {
    FunctionTypeMismatchException
}