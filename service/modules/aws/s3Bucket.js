let {ExtendedLogger} = require("@logger");
let stream           = require('stream');
let fs               = require("fs");


class Bucket {

  /*************************************************************************************
    gets a file from s3 bucket
    @param AWS req    => Object of AWS SDK with configuration of programattic access
    @param bucket req => name of the s3 bucket
  */
  constructor(AWS, bucket) {
    this._s3 = new AWS.S3();
    this._bucket = bucket;
    this._logger  = new ExtendedLogger("mod_aws.s3Service");
  }

  set name(bucket) {
    this._bucket = bucket;
  }

  get name() {
    return this._bucket;
  }

  get url() {
    return "s3:\\" + this._bucket;
  }

  /**
    gets a file from s3 bucket
    @functionName getFile
    @param   fileKey req => name/path of the file on the s3 bucket
    @returns promise to return file
  */
  getFile(fileKey) {
    this._logger.debug('getfile', `Getting the file '${fileKey}' from the S3 bucket s3://${this.name}`);
    let s3Params = {
      Bucket: this._bucket,
      Key: fileKey
    };
    let self = this;
    return new Promise(function(resolve, reject) {
      self._s3.getObject(s3Params, function(err, fileData) {
          if (err === null) {
            resolve(fileData);
          } else {
             self._logger.debug(err);
             reject(err);
          }
      });
    })
  }
  /**
    gets a file from s3 bucket
    @functionName saveFile
    @param  localFileOrSteam req => name/path of the local file or steam
    @param  fileKey req => name/path of the file on the s3 bucket
    @returns promise to return response from s3
  */

  saveFile(localFileOrSteam, fileKey) {
      // console.log("I am called");
      let self = this;
      const { writeStream, promise } = this.uploadStream({Bucket: this._bucket, Key: fileKey});
      return new Promise(async function(resolve, reject) {
        if(typeof localFileOrSteam == "string") {
          fs.exists(localFileOrSteam, function(fileExists) {
            if (fileExists) {
                //stream
                self._logger.debug("Uploading file steam");
                const readStream = fs.createReadStream(localFileOrSteam);
                readStream.pipe(writeStream);
            } else {
              self._logger.error(`File '${localFileOrSteam}' does not exists`);
              return reject(`File '${localFileOrSteam}' does not exists`);
            }
          });
        } else {
          self.uploadStream(readStream, fileKey);
        }
    })
  }
  /**
    @params Object with Bucket and Key
  */
  uploadStream({Bucket, Key}) {

    let pass  = new stream.PassThrough();
    let manager = this._s3.upload({ Bucket, Key, Body: pass })
    let self    = this;

    manager.on('httpUploadProgress', (progress) => {
      self._logger.log('progress', progress) // { loaded: 4915, total: 192915, part: 1, key: 'foo.jpg' }
    });
    return {
      writeStream: pass,
      promise: manager.promise(),
    };
  }

} //end class

module.exports = Bucket;
