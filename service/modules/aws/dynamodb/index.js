const aws                = require('aws-sdk');
const { ExtendedLogger } = require("@logger");
const config             = require('config');
const dynamoDbConstants  = config.get("dynamodb");
const { DynamoHelper }   = require('./helper');

/**
 * The dynamo db class to handle all connections with create and fetch request
 */
class DynamoDb {

    /**
     * Constructor for dynamodb connector
     * @param {String} name Name of dynamodb table config to use
     * @author Swapnil Kumar (techgenies.com)
     */
    constructor(name){
        this.logger = new ExtendedLogger('Dynamo Db class', null);
        this.logger.debug("dynamo db table name",name);
        this._isOperational = true;
        if(typeof(name) == "undefined"){
            this._isOperational = false;
            this.logger.error("dynamodb is not operational");
            return;
        }
        this.dynamoDbHelper = new DynamoHelper();
        this.constants = dynamoDbConstants[name];
        this.ddb = this.dynamoDbHelper.createDynamodbDocumentClient(aws, this.constants.dynamodbRegion, this.constants.dynamodbApiVersion, 
            this.constants.dynamodbAccessKeyId, this.constants.dynamodbSecretAccessKey);
        this.logger.log("Successfully created dynamodb connection");
        this.logger.debug("Successfully created dynamodb connection");
        this.tableName = this.constants.dynamodbTable;
    }

    /**
     * This function will make a putItem call by consuming the apis exposed by aws-sdk
     * @param {Object} item Object containing information to be added to dynamodb
     * @param {String} tableName Name of table to make entry into
     * @param {String} region AWS region in which dynamo db instance is hosted
     * @returns {Promise} Returns a promise containing the result recieved from dynamodb call
     * @author Swapnil Kumar (techgenies.com)
     */
    create(item){

        if(!this._isOperational){
            this.logger.error("dynamodb is not operational");
            return;
        }
        this.logger.debug("Entered function",item);
        let params = this.dynamoDbHelper.connectionPostParam(item, this.constants.dynamodbReturnConsumedCapacity, 
            this.constants.dynamodbReturnValues, this.constants.dynamodbReturnItemCollectionMetrics, this.tableName);
        
        return new Promise((resolve,reject) => {
            this.ddb.put(params, (err, data) => {
                if (err) {
                    this.logger.error("create()", err);
                    this.logger.debug("create()", err);
                    reject(err);
                }
                else{
                    this.logger.debug("create()", data);
                    resolve(data);
                }
            });
        });
    }

    /**
     * A function to fetch query from dynamodb
     * @param {Object} searchKeys Keys to search in logs 
     * @param {String} tableName Name of table
     * @param {String} region Name of AWS region in which dynamodb is hosted
     * @author Swapnil Kumar (techgenies.com)
     */
    fetch(searchKeys){
        
        if(!this._isOperational){
            this.logger.error("dynamodb is not operational");
            return;
        }
        this.logger.debug("fetch()", "Entered function",searchKeys);
        let params = this.dynamoDbHelper.connectionGetParam(searchKeys, this.tableName, this.constants.dynamodbSecondaryIndex);
        return new Promise((resolve,reject) => {

            if(params == null){
                reject("null");
                return;
            }

            this.ddb.query(params, (err, data) => {
                if (err) {
                    this.logger.error("fetch()", err);
                    this.logger.log("fetch()", err);
                    reject(err);
                }
                else{
                    this.logger.log("fetch()", data);
                    resolve(data);
                }
            });
        });
    }

}

module.exports = DynamoDb;