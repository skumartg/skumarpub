class DynamoHelper {
    /**
     * This function will return a parameter object formed by input fields
     * @param {Object} item Activity log Item to insert in to dynamodb
     * @param {String} returnConsumedCapacity Consumed capacity to be returned from dynamodb
     * @param {String} returnValues Return values to be feteched after the request from dynamodb
     * @param {String} returnItemCollectionMetrics Item collection metrics after the write requests in dynamodb
     * @param {String} tableName Table name of the dynamodb to write our item
     * @returns {Object} A parameter object containing Item, ReturnConsumedCapacity, TableName, ReturnItemCollectionMetrics, ReturnValues to be used by dynamo db connection to make request on AWS
     * @author Swapnil Kumar(techgenies.com)
     */
    connectionPostParam(item, returnConsumedCapacity, returnValues, returnItemCollectionMetrics, tableName){
        return {
            Item: item,
            ReturnConsumedCapacity: returnConsumedCapacity,
            TableName: tableName,
            ReturnItemCollectionMetrics: returnItemCollectionMetrics,
            ReturnValues: returnValues
        }
    }

    /**
     * This function will fill keyConditionExpressions,filterExpressions,expressionAttributeValues based on searchKeys object recieved in request
     * @param {Object} searchKeys Search keys for dynamodb to fetch data
     * @param {Array} keyConditionExpressions The key condition expressions are those conditions that apply on secondary hash indexes
     * @param {Array} filterExpressions The filter expressions are those conditions that apply on non-index rows
     * @param {Array} expressionAttributeValues Expression attribute values are the values related to tags written in expressions
     * @author Swapnil Kumar(techgenies.com)
     */
    createAttributesForGetParameter(searchKeys,keyConditionExpressions,filterExpressions,expressionAttributeValues){
        Object.keys(searchKeys).map((key,index) => {

            if(key.startsWith('key_')){
                let tempKey = key.split("key_")[1];
                keyConditionExpressions.push(`${tempKey} = :key${index}`);
                expressionAttributeValues[`:key${index}`] = searchKeys[key];
            } else if (key.startsWith('filter_')) {
                let tempKey = key.split("filter_")[1];
                filterExpressions.push(`contains(${tempKey}, :filter${index})`);
                expressionAttributeValues[`:filter${index}`] = searchKeys[key];
            }
        });
    }

    /**
     * This function will return a search parameter object formed by input fields
     * @param {Object} searchKeys Search keys for dynamo db to fetch data
     * @param {String} tableName Name of the dynamodb table
     * @returns {Object} A parameter object containing FilterExpression and TableName
     * @author Swapnil Kumar(techgenies.com)
     */
    connectionGetParam(searchKeys, tableName, index){
        let expressionAttributeValues = {};
        let keyConditionExpressions = [];
        let filterExpressions = [];
        
        this.createAttributesForGetParameter(searchKeys, keyConditionExpressions, filterExpressions, expressionAttributeValues);

        if(keyConditionExpressions.length == 0){
            return null;
        }

        let params = {
            TableName: tableName,
            KeyConditionExpression: keyConditionExpressions.join(' and '),
            ExpressionAttributeValues: expressionAttributeValues
        };

        index != null ? params["IndexName"] = index :null;
        filterExpressions.length > 0 ? params.FilterExpression = filterExpressions.join(' or ') : null;

        return params;
    }

    /**
     * This function is used to create dynamo db document client to make item level query using aws-sdk
     * @param {String} region Region os AWS in which dynamodb is hosted
     * @author Swapnil Kumar(techgenies.com)
     */
    createDynamodbDocumentClient(aws, region, apiVersion, accessKeyId, secretAccessKey){
        return new aws.DynamoDB.DocumentClient({region, apiVersion, accessKeyId, secretAccessKey});
    }
}

module.exports = {
    DynamoHelper
}