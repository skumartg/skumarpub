/**
  Module to call functions for different Services using AWS SDK
  @dependencies aws-sdk
  @exports objects of different services included in individal files
  @author Gaurav Gambhir - TechGenies (gaurav.gambhir@TechGenies.com)
*/

let {ExtendedLogger} = require("@logger");
const CONFIG         = require("config");
const AWS            = require('aws-sdk');
const AWS_ACCESS_ID  = CONFIG.get("aws.accessId");
const AWS_SECRET_KEY = CONFIG.get("aws.secretKey");
const AWS_S3_BUCKET  = CONFIG.get("aws.s3.bucket");

let awsLogger  =  new ExtendedLogger("mod_aws");
/**
  Configure AWS
*/
AWS.config.update({
    accessKeyId: AWS_ACCESS_ID,
    secretAccessKey: AWS_SECRET_KEY
})
let Bucket = require("./s3Bucket");
let defaultBucket = new Bucket(AWS,AWS_S3_BUCKET);

module.exports = {Bucket, defaultBucket}
