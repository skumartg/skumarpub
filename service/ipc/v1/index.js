const route      = require("express").Router();
const bodyParser = require("body-parser");


route.get("/test", function(req, res) {
  setTimeout(function() {
      res.status(200).send("IPC Version V1 working");
  }, 3000);

})

route.post("/test", function(req, res) {
  // req.log("THIS IS A TEST", "warn");
  res.status(200).send("Public Version V1 working");
})

module.exports = route;
