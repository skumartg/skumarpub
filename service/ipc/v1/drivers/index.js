const {ExtendedLogger} = require('@logger');

var normalizedPath = require("path").join(__dirname);
var Models = {};
require("fs").readdirSync(normalizedPath).forEach(function(file) {
  if (file != "index.js" && file.substring(file.length - 3).toLowerCase() == ".js" ) {
    var fileName = file.substring(0,file.length - 3);
    var temp = fileName.split("_");
    var tempString = "";
    if (temp[0] == "driver") {
      for (var i=1;i<temp.length;i++) {
          tempString += temp[i].charAt(0).toUpperCase() + temp[i].substring(1).toLowerCase();
      }
    }

    fileNameVariable = tempString;
    Models[fileNameVariable] = require("./" + file);

  };
});

/**
 * Function to start listening to each and every IPC function
 */
const start = () => {
    let logger = new ExtendedLogger("/v1/drivers/index.js_start()", null);
    logger.debug("Starting drivers");
    Object.keys(Models).map((fn) => {
        Models[fn].start();
        logger.debug(`Listening to ${fn} queue via SQS`);
    });
    logger.debug("Listening to all queues");
}

/**
 * Function to stop listening to each and every IPC function
 */
const stop = () => {
    let logger = new ExtendedLogger("/v1/drivers/index.js_stop()", null);
    logger.debug("Stopping drivers");
    Object.keys(Models).map((fn) => {
        Models[fn].stop();
        logger.debug(`Unsubscribed to ${fn} queue via SQS`);
    });
    logger.debug("Stopped listening to all queues");
}
module.exports = {start,stop};