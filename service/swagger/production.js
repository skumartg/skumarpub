/**
 * This is the basic swagger definition, here we can modify basic things for swagger documentation.
 * If we need to add any definition model to swagger then it can be done in ./definitions.json file
 */
const definitions = require('./definitions.json');
var swaggerDefinition = {
  "swagger": "2.0",
  "info": swaggerConst.info,
  "host": swaggerConst.host,
  "tags": [
    {
      "name":"Public",
      "description":"APIs exposed to public or api gateway"
    },
    {
      "name":"IPC",
      "description":"APIs for synchronous internal-process communication"
    }
  ], 
  "schemes": [
    "http"
  ],
  "definitions":definitions
};

// options for the swagger docs
module.exports = {
  // import swaggerDefinitions
  swaggerDefinition: swaggerDefinition,
  // path to the API docs relative to app.js
  apis: ['./public/**/*.js','./ipc/**/*.js']
};

