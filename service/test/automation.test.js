var modules = require('@function-metadata')["modules"];
const path = require('path');

/**
 * This list contains positive and negative test value for each data type in application, at index 1 -> paramType , at index 1 -> negative , at index 2 -> positive
 */
const dataTypeTestList = {
    "undefined":["number",234,undefined],
    "symbol":["number",234,Symbol(42)],
    "string":["object",{"key":"value"},"abcdefg"],
    "object":["number",234,{"key":"value"}],
    "number":["string","abcdefg",234],
    "function":["object",{"key":"value"},() => {}],
    "boolean":["string","abcdefg",true],
    "?symbol":["number",234,Symbol(42)],
    "?string":["object",{"key":"value"},null],
    "?object":["number",234,{"key":"value"}],
    "?number":["string","abcdefg",234],
    "?function":["object",{"key":"value"},() => {}],
    "?boolean":["string","abcdefg",null]
};

/**
 * Returns array of function names to test
 * @param {Object} moduleMetadata Metadata of modules written in ./function-metadata.json
 */
const getFunctionsList = (moduleMetadata) => {
    let functions = [];
    Object.keys(moduleMetadata).map((key) => {

        if(key.startsWith("fn_")){
            functions.push({name:key.split("_")[1], paramTypes:moduleMetadata[key]});
        }

    })

    return functions;
}

/**
 * A function to capture modules metadata information
 * @param {Object} modules An object of modules to run tests upon
 * @returns {Array} An array of modules metadata information
 */
const getModulesList = (modules) => {
    let moduleList = [];
    Object.keys(modules).map((moduleKey) => {
        let temp = modules[moduleKey];
        temp.name = moduleKey;
        moduleList.push(temp);
    })
    return moduleList;
}

/**
 * This function will read the function-metadata.json containing functions metadata. It will then generate positive and negative test cases based on type checking.
 */
const generate = () => {

    modules = getModulesList(modules);

    if(modules.length == 0){
        test(`No test modules found`,() => {
        });
    }

    modules.map((moduleMetadata) => {
        const testModule = require(path.join('..',moduleMetadata.path));
        const functions = getFunctionsList(moduleMetadata);

        describe(`Test Module: ${moduleMetadata.name} , Path: ${moduleMetadata.path}`,()=>{
            
            functions.map((functionMetadata) => {
                let params = [];

                functionMetadata.paramTypes.map((type) => {
                    params.push(dataTypeTestList[type][2]);
                });
                
                //positive test
                test(`POS Function: ${functionMetadata.name}(), type checking with parameters ${JSON.stringify(params)}`,() => {
                    expect(()=>{
                        testModule[functionMetadata.name].apply(this,params);
                    }).not.toThrow();
                });
                
                //negative tests
                functionMetadata.paramTypes.map((type,index) => {
                    console.log("here");
                    let modifiedParams = [...params];
                    modifiedParams[index] = dataTypeTestList[type][1];
                    test(`NEG Function: ${functionMetadata.name}(), type checking with parameters ${JSON.stringify(modifiedParams)}`,() => {
                        expect(()=>{
                            testModule[functionMetadata.name].apply(this,modifiedParams)
                        }).toThrow();
                    });
                });
                
            });
        });
    });
}

//Function call to run basic type checking level test cases
generate();