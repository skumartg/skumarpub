var route = require("express").Router();
const {validate} = require("@api-schema-validator");

/**
 * @swagger
 * /api/v1/test:
 *   get:
 *     tags:
 *     - "Public"
 *     summary: "Test get api"
 *     description: "This a sample test api for microservice template"
 *     consumes:
 *     - "application/json"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - in: "query"
 *       name: "param"
 *       type: "string"
 *       description: "Example parameter"
 *       required: false
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       500:
 *         description: "Internal server error"
 */
route.get("/test", validate({
  "query" : [{
    "param" : {
      "$req" : true
    }
  }]
}), function(req, res) {
  res.status(200).send("Publiac Version V1 working, returning "+req.query.param);
})

/**
 * @swagger
 * /api/v1/test:
 *   post:
 *     tags:
 *     - "Public"
 *     summary: "Test get api"
 *     description: "This a sample test api for microservice template"
 *     consumes:
 *     - "application/json"
 *     produces:
 *     - "application/json"
 *     parameters:
 *     - in: "body"
 *       name: "param"
 *       type: "string"
 *       description: "Example parameter"
 *       required: false
 *       schema:
 *         $ref: "#/definitions/Example_Model"
 *     responses:
 *       400:
 *         description: "Invalid input"
 *       500:
 *         description: "Internal server error"
 */
route.post("/test", validate({
  "body" : [{
    "l1" : {
      "l2" : {
        "$req" : true,
        "$type":"number"
      }
    }
  }]
}), function(req, res) {
  res.status(200).send("Publiac Version V1 working, returning "+Object(req.body.toString()));
})


module.exports = route;
