var route = require("express").Router();

route.use("/:version", function(req, res, next) {
  require(`./${req.params.version}/index.js`)(req, res, next);
});

module.exports = route;
