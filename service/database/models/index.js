var normalizedPath = require("path").join(__dirname);
var Models = {};
require("fs").readdirSync(normalizedPath).forEach(function(file) {
  if (file != "index.js" && file.substring(file.length - 3).toLowerCase() == ".js" ) {
    var fileName = file.substring(0,file.length - 3);
    var temp = fileName.split("_");
    var tempString = "";
    if (temp[0] == "model") {
      for (var i=1;i<temp.length;i++) {
          tempString += temp[i].charAt(0).toUpperCase() + temp[i].substring(1).toLowerCase();
      }
    }

    fileNameVariable = "m"+tempString;
    exports[fileNameVariable] = require("./" + file);

  };
});
