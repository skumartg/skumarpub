let {ExtendedLogger} = require("../modules/logger");
var dbLogger = new ExtendedLogger("db");

const Sequelize = require('sequelize');
const sequelize = new Sequelize(process.env.DATABASE_HOST, process.env.DATABASE_USER, process.env.DATABASE_PASSWORD, {
  host: 'db',
  dialect: 'postgres',
  pool: {
    max: 50,
    min: 0,
    idle: 10000
  }
  ,logging:false,
});
sequelize.authenticate().then(() => {
    dbLogger.log('Connection has been established successfully.');
  })
  .catch(err => {
    dbLogger.log('Unable to connect to the database: ' + err , "error");
  });

module.exports = sequelize;
