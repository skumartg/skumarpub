FROM node:10.16.3

COPY ./service /service
COPY ./waitForPGSQL.dev.sh /autoExec/waitForPGSQL.dev.sh
WORKDIR /service

RUN npm install nodemon -g
RUN npm install

RUN chmod +x /autoExec/waitForPGSQL.dev.sh

ENV PORT 80
EXPOSE 80

CMD ["/autoExec/waitForPGSQL.dev.sh"]